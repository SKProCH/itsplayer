﻿using System.Collections.Generic;
using System.Linq;
using CSCore.CoreAudioAPI;

namespace ItsPlayer.Model {
    public class MMDeviceWrapper {
        public MMDeviceWrapper(MMDevice device) {
            Device = device;
        }

        public MMDeviceWrapper(MMDevice device, string displayName) {
            Device = device;
            DisplayName = displayName;
        }
        
        public MMDevice Device { get; set; }
        public string DisplayName { get; set; }
        public string DeviceName => string.IsNullOrWhiteSpace(DisplayName) ? Device.FriendlyName : DisplayName;
        
        public override string ToString() {
            return DeviceName;
        }
    }

    public class AudioHelper {
        public static MMDeviceWrapper GetOutputDevice(List<MMDeviceWrapper> outputDevicesList = null) {
            outputDevicesList ??= GetOutputDevices();
            var selectedOutput = //outputDevicesList.FindIndex(wrapper => wrapper.Device?.DeviceID == GlobalConfig.Instance.OutputDeviceId); 
                -1;
            return selectedOutput != -1 ? outputDevicesList[selectedOutput] : outputDevicesList.First(wrapper => wrapper.DeviceName.StartsWith("DEF: "));
        }

        public static List<MMDeviceWrapper> GetOutputDevices() {
            var toReturn = new List<MMDeviceWrapper>();
            using var mmdeviceEnumerator = new MMDeviceEnumerator();

            var outputDevices = mmdeviceEnumerator.EnumAudioEndpoints(DataFlow.Render, DeviceState.Active).OrderBy(device => device.FriendlyName).ToList();
            var defaultOutputDevice = mmdeviceEnumerator.GetDefaultAudioEndpoint(DataFlow.Render, Role.Console);
            foreach (var device in outputDevices.Select(t => new MMDeviceWrapper(t))) {
                if (device.Device.DeviceID == defaultOutputDevice.DeviceID) {
                    device.DisplayName = "DEF: " + device.Device.FriendlyName;
                }

                toReturn.Add(device);
            }

            return toReturn;
        }
    }
}