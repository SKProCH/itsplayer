﻿using System.Windows;
using ItsPlayer.ViewModel;

namespace ItsPlayer {
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App {
        protected override void OnStartup(StartupEventArgs e) {
            var mw = new MainWindow{DataContext = new MainWindowVM()};
            mw.Show();
        }
    }
}