﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ItsPlayer.Model.Converters {
    public class TimeSpanToSeconds : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value is TimeSpan span) {
                return span.TotalSeconds;
            }

            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value is double i) {
                return TimeSpan.FromSeconds(i);
            }

            return TimeSpan.Zero;
        }
    }
}