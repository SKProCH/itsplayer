﻿using System;
using System.ComponentModel;
using CSCore;
using CSCore.Codecs;
using CSCore.CoreAudioAPI;
using CSCore.SoundOut;

namespace ItsPlayer.Model {
    public class MusicPlayer : Component {
        private ISoundOut _soundOut;
        private IWaveSource _waveSource;
        private bool _initialized;

        public event EventHandler<PlaybackStoppedEventArgs> PlaybackStopped;

        public bool Initialized {
            get => _initialized;
            private set {
                _initialized = value; 
                InitialedStateChanged?.Invoke(this, new EventArgs());
            }
        }

        public event EventHandler InitialedStateChanged;

        public PlaybackState PlaybackState => _soundOut?.PlaybackState ?? PlaybackState.Stopped;

        public TimeSpan Position {
            get => _waveSource?.GetPosition() ?? TimeSpan.Zero;
            set => _waveSource?.SetPosition(value);
        }

        public TimeSpan Length => _waveSource?.GetLength() ?? TimeSpan.Zero;

        public int Volume {
            get => _soundOut != null ? Math.Min(100, Math.Max((int) (_soundOut.Volume * 100), 0)) : 100;
            set {
                if (_soundOut != null) {
                    _soundOut.Volume = Math.Min(1.0f, Math.Max(value / 100f, 0f));
                }
            }
        }

        public void Open(string filename) {
            CleanupPlayback();
            
            _waveSource =
                CodecFactory.Instance.GetCodec(filename)
                    .ToSampleSource()
                    .ToMono()
                    .ToWaveSource();
            _soundOut?.Initialize(_waveSource);
            Initialized = true;
			if (PlaybackStopped != null && _soundOut != null) _soundOut.Stopped += PlaybackStopped;
        }

        public void SetOutput(MMDevice device) {
            _soundOut?.Dispose();
            _soundOut = new WasapiOut() {Latency = 100, Device = device};
            if (_waveSource != null)
                _soundOut?.Initialize(_waveSource);
            Initialized = _waveSource != null;
        }

        public void Play() {
            _soundOut?.Play();
        }

        public void Pause() {
            _soundOut?.Pause();
        }

        public void Stop() {
            _soundOut?.Stop();
        }

        private void CleanupPlayback() {
            // if (_soundOut != null) {
            //     _soundOut.Dispose();
            //     _soundOut = null;
            // }

            if (_waveSource != null) {
                _waveSource.Dispose();
                _waveSource = null;
            }
        }

        protected override void Dispose(bool disposing) {
            base.Dispose(disposing);
            CleanupPlayback();
        }
    }
}