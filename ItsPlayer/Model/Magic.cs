﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ItsPlayer.Model {
    internal sealed class MagicAttribute : Attribute { }

    internal sealed class NoMagicAttribute : Attribute { }
    
    [Localizable(false)]
    [Magic]
    public abstract class PropertyChangedBase : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;

        [Localizable(false)]
        protected virtual void RaisePropertyChanged([CallerMemberName] string propName = null) {
            var e = PropertyChanged;
            e?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}