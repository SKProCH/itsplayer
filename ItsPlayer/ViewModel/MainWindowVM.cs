﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows;
using CSCore.Codecs;
using CSCore.SoundOut;
using ItsPlayer.Model;
using Microsoft.Win32;

namespace ItsPlayer.ViewModel {
    [Magic]
    public class MainWindowVM : PropertyChangedBase {
        public MainWindowVM() {
            OutputDeviceList = AudioHelper.GetOutputDevices();
            var selectedDevice = AudioHelper.GetOutputDevice(OutputDeviceList);
            SelectedOutputDevice = OutputDeviceList.Find(wrapper => wrapper.Device.DeviceID == selectedDevice.Device.DeviceID);

            _updateTimer = new Timer(state => {
                if (Player.PlaybackState != PlaybackState.Playing) return;
                ProgressMaximum = Player.Length;
                ProgressValue = Player.Position;
            }, null, TimeSpan.FromSeconds(0), TimeSpan.FromMilliseconds(100));
            Player.InitialedStateChanged += (sender, args) => { IsInitialized = Player.Initialized; };
        }

        private Timer _updateTimer;
        public List<MMDeviceWrapper> OutputDeviceList { get; set; }

        public MMDeviceWrapper SelectedOutputDevice {
            get => _selectedOutputDevice;
            set {
                try {
                    Player.SetOutput(value.Device);
                    _selectedOutputDevice = value;
                }
                catch (Exception) {
                    Player.SetOutput(_selectedOutputDevice.Device);
                    MessageBox.Show($"Что то пошло не так. Вы не можете использовать это устройство: {value.DeviceName}");
                }
                
                // TODO: Implement wrong item select rollback
            }
        }
        
        public MusicPlayer Player { get; set; } = new MusicPlayer();
        private MMDeviceWrapper _selectedOutputDevice;
        private TimeSpan _progressValue;
        private int _volumeLevel = 100;

        public string TrackTitle { get; set; }

        public TimeSpan ProgressMaximum { get; set; }

        public TimeSpan ProgressValue {
            get => _progressValue;
            set {
                _progressValue = value;
                if (Player.PlaybackState != PlaybackState.Playing) {
                    Player.Position = value;
                }

                UpdateTrackPositionText();
            }
        }

        public AnotherCommandImplementation Pause =>
            new AnotherCommandImplementation(o => { Player.Pause(); });

        public AnotherCommandImplementation Play =>
            new AnotherCommandImplementation(o => { Player.Play(); });

        public AnotherCommandImplementation Stop =>
            new AnotherCommandImplementation(o => { Player.Stop(); });

        public AnotherCommandImplementation Open =>
            new AnotherCommandImplementation(o => {
                var openFileDialog = new OpenFileDialog {Multiselect = false, Filter = CodecFactory.SupportedFilesFilterEn};
                var dialogSuccess = openFileDialog.ShowDialog();
                if (dialogSuccess != true) return;
                Player.Open(openFileDialog.FileName);
                TrackTitle = Path.GetFileNameWithoutExtension(openFileDialog.FileName);
            });

        private void UpdateTrackPositionText() {
            var sb = new StringBuilder("");
            if ((int) ProgressValue.TotalHours != 0)
                sb.Append((int) ProgressValue.TotalHours + ":");
            sb.Append($"{ProgressValue:mm':'ss} / ");
            if ((int) ProgressMaximum.TotalHours != 0)
                sb.Append((int) ProgressMaximum.TotalHours + ":");
            sb.Append($"{ProgressMaximum:mm':'ss}");
            TrackPositionText = sb.ToString();
        }

        public string TrackPositionText { get; set; }

        public bool IsInitialized { get; set; }

        public int VolumeLevel {
            get => _volumeLevel;
            set {
                _volumeLevel = value;
                Player.Volume = value;
            }
        }
    }
}