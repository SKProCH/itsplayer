﻿using System.Windows.Input;
using ItsPlayer.ViewModel;

namespace ItsPlayer {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow {
        public MainWindow() {
            InitializeComponent();
        }

        private void UIElement_OnPreviewMouseDown(object sender, MouseButtonEventArgs e) {
            if (this.DataContext is MainWindowVM vm) {
                vm.Player.Pause();
            }
        }

        private void UIElement_OnPreviewMouseUp(object sender, MouseButtonEventArgs e) {
            if (this.DataContext is MainWindowVM vm) {
                vm.Player.Play();
            }
        }
    }
}